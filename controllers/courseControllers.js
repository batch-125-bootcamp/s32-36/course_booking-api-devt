const Course = require("./../models/courses")

// retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}


module.exports.getAllCourses = () => {
	return Course.find().then(result => {
		return result
	})
}

// add course
module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, err) => {
		if(err){
			return false
		} else {
			return true
		}
	});
};

module.exports.getSingleCourse = (params) => {
	console.log(params)
	return Course.findById(params.courseId).then(result => {
		return result
	})
};

module.exports.editCourse = (params, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	}
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true}).then((result, err) => {
		if(err) {
			return err
		} else {
			return result
		}
	})
}

module.exports.archiveCourse = (params) => {

	let updatedActiveCourse = {
		isActive: false
	}
	return Course.findByIdAndUpdate(params, updatedActiveCourse, {new: true}).then((result, err) => {
		if (err){
			return false
		} else 
			return true
	})
}

module.exports.unarchiveCourse = (params) => {

	let updatedInactiveCourse = {
		isActive: true
	}
	return Course.findByIdAndUpdate(params, updatedInactiveCourse, {new: true}).then((result, err) => {
		if (err){
			return false
		} else 
			return true
	})
}

module.exports.deleteCourse = (params) => {
	return Course.findByIdAndDelete(params).then((result, err) => {
		if (err){
			return false
		} else 
			return true
	})
}