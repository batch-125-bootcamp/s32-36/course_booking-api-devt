// we will use this for authorization
	// login
	// retrieve user details

// JSON Web Token
	// methods:
		// sign(data, secret, {options})
			// creates a token
		// verify(token, secret, cb())
			// checks if the token is present
		// decode(token, {}).payload
			// interpret the created token

let jwt = require('jsonwebtoken');
let secret = "CourseBookingAPI";

// create a token


module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret);
}


// verify token

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
		
	 /*bearer fhajjq29u21903jwdjksl is in authorization*/

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length)
				// console.log(token)
		return jwt.verify(token, secret, (err, data) =>{
			if(err){
				return res.send({auth: "failed"})
			} else {
				next(); /*proceed to the next step*/
			}
		})
	}
}

// decode token
// group act (sir dave, sir robert, mam miah and me)

module.exports.decode = (token) => {

		if(typeof token !== "undefined"){
			token = token.slice(7, token.length);
			return jwt.decode(token, {complete: true}).payload;
		}
};