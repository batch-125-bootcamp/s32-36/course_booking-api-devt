
const express = require("express")
const mongoose = require("mongoose")
const cors = require('cors')

const port = process.env.PORT || 3000 ;
const app = express();

let userRoutes = require("./routes/userRoutes")
let courseRoutes = require("./routes/courseRoutes")

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

mongoose.connect("mongodb+srv://09CGarcia:patatasako23@cluster0.sjmpx.mongodb.net/course_booking?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
).then(() => console.log(`Connected to Database! Yay!`))
.catch((err) => console.log(err));

app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);
app.listen(port, () => console.log(`Server is running at ${port}`));